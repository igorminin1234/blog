package com.example.blog.entities;

import lombok.Getter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "articles")
@Getter
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private String heading;
    private String body;
    private String author;
    private LocalDate publicationDate;

    protected Article() {}

    public Article(final String heading, final String body, final String author, final LocalDate publicationDate) {
        this.heading = heading;
        this.body = body;
        this.author = author;
        this.publicationDate = publicationDate;
    }
}
