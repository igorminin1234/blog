package com.example.blog.entities;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "comments")
@Getter
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String username;

    private String text;

    @Column(name = "article_id")
    private long articleId;

    protected Comment() {
    }

    public Comment(final String username, final String text, final long articleId) {
        this.username = username;
        this.text = text;
        this.articleId = articleId;
    }
}
