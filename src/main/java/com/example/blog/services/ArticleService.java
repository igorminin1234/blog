package com.example.blog.services;

import com.example.blog.entities.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ArticleService {
    public void createArticle(final Article article);

    public Article extractArticle(final long id);

    public Page<Article> extractArticles(final Pageable pageable);

    public void updateArticle(final long id, final String heading, final String body);

    public void deleteArticle(final long id);
}
