package com.example.blog.services;

import com.example.blog.entities.Article;
import com.example.blog.repository.ArticleRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ArticleServiceImpl implements ArticleService {

    final ArticleRepository ar;

    public ArticleServiceImpl(final ArticleRepository ar) {
        this.ar = ar;
    }

    /**
     * Add new article into DB
     * @param article - value object of Article you want to add into DB
     */
    @Override
    public void createArticle(final Article article) {
        ar.save(article);
    }

    /**
     * Extract article from DB
     * @param id - id of article you want to extract from DB
     * @return article by id
     */
    @Override
    public Article extractArticle(final long id) {
        return ar.getById(id);
    }

    /**
     * Extract all articles from DB
     * @param pageable - object using for pagination
     * @return pages with articles
     */
    @Override
    public Page<Article> extractArticles(final Pageable pageable) {
        return ar.findAll(pageable);
    }

    /**
     * Update article in DB
     * @param id - id of article you want to update
     * @param heading - new heading for article you want to update
     * @param body - new body for article you want to update
     */
    @Override
    public void updateArticle(final long id, final String heading, final String body) {
        ar.updateArticle(id, heading, body);
    }

    /**
     * Delete article from DB
     * @param id - id of article you want to delete from DB
     */
    @Override
    public void deleteArticle(final long id) {
        ar.deleteById(id);
    }
}
