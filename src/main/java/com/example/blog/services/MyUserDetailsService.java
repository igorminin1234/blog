package com.example.blog.services;

import com.example.blog.entities.User;
import com.example.blog.repository.UserRepository;
import com.example.blog.services.data.MyUserDetails;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

    final UserRepository ur;

    public MyUserDetailsService(final UserRepository ur) {
        this.ur = ur;
    }

    @Override
    public UserDetails loadUserByUsername(final String s) throws UsernameNotFoundException {
        final Optional<User> maybeUser = ur.findUserByUsername(s);
        if (maybeUser.isEmpty()) {
            throw new UsernameNotFoundException("Could not find user");
        }

        return new MyUserDetails(maybeUser.get());
    }
}
