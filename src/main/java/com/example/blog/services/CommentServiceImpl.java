package com.example.blog.services;

import com.example.blog.entities.Comment;
import com.example.blog.repository.CommentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {

    final CommentRepository cr;

    public CommentServiceImpl(final CommentRepository cr) {
        this.cr = cr;
    }

    /**
     * Add new comment into DB
     * @param comment - value object of comment you want to add into DB
     */
    @Override
    public void createComment(final Comment comment) {
        cr.save(comment);
    }

    /**
     * Extract all comments for article from DB
     * @param pageable - object using for pagination
     * @param article_id - id of article you want comment
     * @return Pages of comments
     */
    @Override
    public Page<Comment> extractComments(final Pageable pageable, final Long article_id) {
        return cr.findAllByArticleId(pageable, article_id);
    }

    /**
     * Update comment in DB
     * @param id - id of comment in DB
     * @param text - new text you want to change in comment in DB
     */
    @Override
    public void updateComment(final long id, final String text) {
        cr.updateArticle(id, text);
    }

    /**
     * Delete comment from DB
     * @param id - id of comment in DB
     */
    @Override
    public void deleteComment(final long id) {
        cr.deleteById(id);
    }
}
