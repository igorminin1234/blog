package com.example.blog.services;

import com.example.blog.entities.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CommentService {

    public void createComment(final Comment comment);

    public Page<Comment> extractComments(final Pageable pageable, final Long article_id);

    public void updateComment(final long id, final String text);

    public void deleteComment(final long id);

}
