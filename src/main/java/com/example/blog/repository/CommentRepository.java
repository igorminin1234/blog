package com.example.blog.repository;

import com.example.blog.entities.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    Page<Comment> findAllByArticleId(final Pageable pageable, final long articleId);

    @Transactional
    @Modifying
    @Query("update Comment set text = :text where id = :id")
    void updateArticle(@Param(value = "id") final long id, @Param(value = "text") final String text);
}
