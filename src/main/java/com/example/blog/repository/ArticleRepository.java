package com.example.blog.repository;

import com.example.blog.entities.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;

import javax.transaction.Transactional;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    @NonNull
    Page<Article> findAll(@NonNull final Pageable pageable);

    @Transactional
    @Modifying
    @Query("update Article set heading = :heading, body = :body where id = :id")
    void updateArticle(@Param(value = "id") final long id, @Param(value = "heading") final String heading, @Param(value = "body") final String body);
}
