package com.example.blog.controller;

import com.example.blog.services.ArticleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/")
@Controller
public class MainController extends AbstractController{

    public MainController(final ArticleService articleService) {
        super(articleService);
    }

    @GetMapping
    public String openMainPage() {
        return MAIN_PAGE;
    }


}
