package com.example.blog.controller;

import com.example.blog.entities.Comment;
import com.example.blog.services.ArticleService;
import com.example.blog.services.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.stream.IntStream;

@RequestMapping("/article/{article_id}/")
@Controller
public class CommentController extends AbstractController {

    final Logger logger = LoggerFactory.getLogger(CommentController.class);

    final CommentService commentService;

    public CommentController(final CommentService commentService, final ArticleService articleService) {
        super(articleService);
        this.commentService = commentService;
    }

    private Page<Comment> refreshComments(final Integer page, final Long article_id) {
        return commentService.extractComments(
                PageRequest.of(page, 3, Sort.by(Sort.Direction.DESC, "id")),
                article_id);
    }

    /**
     * Fill model with attributes for display comments of article
     * @param model - model you want to fill
     * @param page - number of page with comments you want to extract from DB
     */
    protected void fillCommentModel(final Model model, final Integer page, final Long article_id) {
        final Page<Comment> commentsPages = refreshComments(page, article_id);
        final int[] numberOfPages = IntStream.range(0, commentsPages.getTotalPages()).toArray();

        model.addAttribute("article_id", article_id);
        model.addAttribute("comments", refreshComments(page, article_id));
        model.addAttribute("numbers", numberOfPages);
    }

    @PostMapping("/comment/create")
    public String createNewComment(final Model model,
                                   @PathVariable(name = "article_id") final Long article_id,
                                   @RequestParam(name = "user", required = false, defaultValue = "Ihor") final String username,
                                   @RequestParam(name = "text") final String text,
                                   @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page) {

        logger.trace("post to /article/" + article_id + "/comment/create");
        final Comment comment = new Comment(username, text, article_id);

        commentService.createComment(comment);

        fillArticleModel(model, page);

        return ARTICLES_PAGE;
    }

    @PostMapping("comment/{id}/delete")
    public String deleteComment(final Model model,
                                @PathVariable(name = "id") final Long id,
                                @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
                                @PathVariable(value = "article_id") final Long article_id) {

        logger.trace("post to /article/" + article_id + "/comment/" + id + "/delete");

        commentService.deleteComment(id);

        fillCommentModel(model, page, article_id);

        return COMMENTS_PAGE;
    }

    @PostMapping("comment/{id}/edit")
    public String editComment(final Model model,
                              @PathVariable(name = "id") final Long id,
                              @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
                              @RequestParam(value = "text") final String text,
                              @PathVariable(value = "article_id") final Long article_id) {

        logger.trace("post to /article/" + article_id + "/comment/" + id + "/edit");

        commentService.updateComment(id, text);

        fillCommentModel(model, page, article_id);

        return COMMENTS_PAGE;
    }


    @GetMapping("/comments")
    public String showAllComments(final Model model,
                                  @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
                                  @PathVariable(value = "article_id") final Long article_id) {

        logger.trace("post to /article/" + article_id + "/comments");

        fillCommentModel(model, page, article_id);

        return COMMENTS_PAGE;
    }

}
