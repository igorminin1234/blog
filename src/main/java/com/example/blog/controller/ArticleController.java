package com.example.blog.controller;

import com.example.blog.entities.Article;
import com.example.blog.services.ArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RequestMapping
@Controller
public class ArticleController extends AbstractController {

    final Logger logger = LoggerFactory.getLogger(ArticleController.class);

    public ArticleController(final ArticleService articleService) {
        super(articleService);
    }

    @GetMapping("/articles")
    public String openArticlePage(final Model model,
                                  @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page) {

        logger.trace("get to /articles");

        fillArticleModel(model, page);
        return ARTICLES_PAGE;
    }

    @PostMapping(value = "/article/{id}/edit")
    public String updateArticle(final Model model,
                                @PathVariable(name = "id") final Long id,
                                @RequestParam(name = "heading") final String heading,
                                @RequestParam(name = "body") final String body,
                                @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page) {

        logger.trace("post to /article/" + id + "/edit");

        articleService.updateArticle(id, heading, body);

        fillArticleModel(model, page);
        return ARTICLES_PAGE;
    }

    @PostMapping(value = "/article/{id}/delete")
    public String deleteArticle(final Model model,
                                @PathVariable(name = "id") final Long id,
                                @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page) {

        logger.trace("post to /article/" + id + "/delete");

        articleService.deleteArticle(id);

        fillArticleModel(model, page);
        return ARTICLES_PAGE;
    }


    @GetMapping("/article/create")
    public String openCreateArticlePage() {

        logger.trace("get to /articles/create");

        return CREATE_ARTICLES_PAGE;
    }

    @PostMapping("/article/create/new")
    public String createNewArticle(@RequestParam(name = "heading") final String heading,
                                   @RequestParam(name = "body") final String body) {

        logger.trace("post to /article/create/new");

        final Article article = new Article(heading, body, "Ihor", LocalDate.now());

        articleService.createArticle(article);

        return MAIN_PAGE;
    }
}
