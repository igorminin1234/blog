package com.example.blog.controller;

import com.example.blog.entities.Article;
import com.example.blog.services.ArticleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;

import java.util.stream.IntStream;

public abstract class AbstractController {

    protected static final String ARTICLES_PAGE = "articles";
    protected static final String MAIN_PAGE = "main";
    protected static final String COMMENTS_PAGE = "comments";
    protected static final String CREATE_ARTICLES_PAGE = "create-article";

    protected final ArticleService articleService;

    public AbstractController(final ArticleService articleService) {
        this.articleService = articleService;
    }

    private Page<Article> refreshArticles(final Integer page) {
        return articleService.extractArticles(
                PageRequest.of(page, 3, Sort.by(Sort.Direction.DESC, "id")));
    }

    /**
     * Fill model with attributes for display articles
     * @param model - model you want to fill
     * @param page - number of page with articles you want to extract from DB
     */
    protected void fillArticleModel(final Model model, final Integer page) {
        final Page<Article> articlesPages = refreshArticles(page);
        final int[] numberOfPages = IntStream.range(0, articlesPages.getTotalPages()).toArray();

        model.addAttribute("articles", refreshArticles(page));
        model.addAttribute("numbers", numberOfPages);
    }
}
