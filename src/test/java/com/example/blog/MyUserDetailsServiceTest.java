package com.example.blog;

import com.example.blog.entities.User;
import com.example.blog.repository.UserRepository;
import com.example.blog.services.MyUserDetailsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MyUserDetailsServiceTest {

    @Autowired
    private MyUserDetailsService uds;

    @Autowired
    private UserRepository ur;

    @Test
    public void loadUserByUsernameTest() {

        ur.save(new User("tester", new BCryptPasswordEncoder().encode("root"), "ROLE_TESTER"));

        UserDetails ud = uds.loadUserByUsername("tester");
        assertEquals(ud.getUsername(), "tester");
        assertTrue(new BCryptPasswordEncoder().matches("root", ud.getPassword()));
        assertEquals(ud.getAuthorities().toArray()[0].toString(), "ROLE_TESTER");
        assertTrue(ud.isAccountNonExpired());
        assertTrue(ud.isAccountNonLocked());
        assertTrue(ud.isEnabled());
        assertTrue(ud.isCredentialsNonExpired());
    }
}
