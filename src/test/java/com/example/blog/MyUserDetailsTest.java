package com.example.blog;

import com.example.blog.entities.User;
import com.example.blog.services.data.MyUserDetails;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class MyUserDetailsTest {

    @Test
    void CreationTest() {
        User user = new User("johnsmith", "123456", "user");
        MyUserDetails myUserDetails = new MyUserDetails(user);
        assertEquals(myUserDetails.getUsername(), "johnsmith");
        assertEquals(myUserDetails.getPassword(), "123456");
        assertEquals(myUserDetails.getAuthorities().toArray()[0].toString(),"user");
        assertTrue(myUserDetails.isAccountNonExpired());
        assertTrue(myUserDetails.isAccountNonLocked());
        assertTrue(myUserDetails.isEnabled());
        assertTrue(myUserDetails.isCredentialsNonExpired());
    }
}
