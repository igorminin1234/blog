package com.example.blog;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
public class ControllersTest {

    @Test
    void testOpenArticlePage(@Autowired MockMvc mvc) throws Exception {
        mvc
                .perform(get("/articles"))
                .andExpect(status().isOk());
    }

    @Test
    void testOpenArticleCreatePage(@Autowired MockMvc mvc) throws Exception {

        mvc
                .perform(get("/article/create")
                        .with(user("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }


    @Test
    void testArticleCreate(@Autowired MockMvc mvc) throws Exception {
        mvc
                .perform(post("/article/create/new")
                        .with(user("admin").roles("ADMIN"))
                        .param("heading", "test-heading")
                        .param("body", "test-body"))
                .andExpect(status().isOk());
    }

    @Test
    void testArticleEdit(@Autowired MockMvc mvc) throws Exception {
        mvc
                .perform(post("/article/1/edit")
                        .with(user("admin").roles("ADMIN"))
                        .param("heading", "test-heading")
                        .param("body", "test-body"))
                .andExpect(status().isOk());
    }


    @Test
    void testOpenMainPage(@Autowired MockMvc mvc) throws Exception {
        mvc
                .perform(get("/"))
                .andExpect(status().isOk());
    }

    @Test
    void testCreateNewComment(@Autowired MockMvc mvc) throws Exception {
        mvc
                .perform(post("/article/1/comment/create")
                        .param("text", "test-text"))
                .andExpect(status().isOk());
    }



    @Test
    void testEditComment(@Autowired MockMvc mvc) throws Exception {
        mvc
                .perform(post("/article/1/comment/1/edit")
                        .param("text", "test-text"))
                .andExpect(status().isOk());
    }

    @Test
    void testShowAllComments(@Autowired MockMvc mvc) throws Exception {
        mvc
                .perform(get("/article/1/comments"))
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteComment(@Autowired MockMvc mvc) throws Exception {
        mvc
                .perform(post("/article/1/comment/1/delete"))
                .andExpect(status().isOk());
    }

    @Test
    void testArticleDelete(@Autowired MockMvc mvc) throws Exception {
        mvc
                .perform(post("/article/2/delete")
                        .with(user("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }
}
